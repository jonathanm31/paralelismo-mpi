#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <iostream>


const int MAX_STRING = 100;
using namespace std;

int main(int argc,char * argv[]){
	int ID, size;
	int ScatterData[4] = {0};
	int ReceiveData = 0;

  	MPI_Init (NULL,NULL);      /* starts MPI */
  	MPI_Comm_rank(MPI_COMM_WORLD,&ID); /*Get Rank */
	MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */

	if(ID == 0){
		ScatterData[0] = 10;
		ScatterData[1] = 20;
		ScatterData[2] = 30;
		ScatterData[3] = 40;

	}

	cout << "Antes Scatter: \n";
	cout << "en Proceso" << ID << "Recibido \t" << endl;

	MPI_Scatter(ScatterData,1,MPI_INT,&ReceiveData,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	cout<< "Despues Scatter: \n";
	cout << "en Proceso" << ID << "REcibido \t" << endl;

	MPI_Finalize();

	return 0;
}